$(document).ready(function () {
  $(".top-promo-carousel").owlCarousel({
    loop: true,
    margin: 10,
    nav: false,
    items: 1,
  });

  $(".owl-carousel.Movies-carousel").owlCarousel({
    rtl: true,
    loop: true,
    margin: 0,
    dots: false,
    items: 5,
    autoWidth: true,
    nav: true,
    responsive: {
      0: {
        items: 2,
      },
      1000: {
        items: 3,
      },
      1200: {
        items: 5,
      },
      1700: {
        items: 8,
      },
      2000: {
        items: 9,
      },
    },
  });

  var hamburger = document.getElementById("nav-icon");
  hamburger.addEventListener("click", function () {
    document.querySelector(".side-nav-list").classList.toggle("slide-in");
    document.querySelector(".hamburger").classList.toggle("close");
  });

  $("#search-input").on("keyup", function (e) {
    if (e.keyCode === 13) {
      let searchVal = $(this).val();

      console.log(searchVal);

      // searchVal value dakhele box search hastesh
    }
  });

  $(".search-btn").on("click", function (e) {
    let searchVal = $(this).siblings("input").val();

    console.log(searchVal);

    // searchVal value dakhele box search hastesh
  });
});
